let map = undefined;
let _activeRoute = undefined;
let _activeLegend = undefined;

document.addEventListener('DOMContentLoaded', (event) => {
    map = L.map('l-map');
    const mapLink = '<a href="http://openstreetmap.org">OpenStreetMap</a>';
    L.tileLayer(
        'https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
        attribution: '&copy; ' + mapLink + ' Contributors',
        maxZoom: 18,
    }).addTo(map);
    _l(map);
})

function _l(map) {
    fetch('files.json')
        .then((response) => {
            return response.json();
        })
        .then((filesArray) => {
            if (filesArray.length) {
                _activeRoute = _g(map, filesArray[0]['lFile']);
                _addSelect(map, filesArray);
            } else console.error('E: invalid Files-Array...');
        })
        .catch((error) => {
            console.error('E: ', error);
        });
}

function _c(className) {
    console.log(className);
    return document.getElementsByClassName(className)[0];
}

function _g(map, gpx) {
    return new L.GPX(gpx, { async: true }).on('loaded', function (e) {
        map.fitBounds(e.target.getBounds());
        _i(map, e.target);
        _hideLoadingIndicator();
    }).addTo(map);
}

function _addSelect(map, filesArray) {
    var legend = L.control({ position: 'topright' });
    legend.onAdd = function (map) {
        var div = L.DomUtil.create('div', 'info legend');
        let dD = '<select onchange="loadGPX(this.options[this.selectedIndex].value);">';
        filesArray.forEach(element => {
            dD += '<option value="' + element.lFile + '">' + element.lName + '</option>';
        });
        dD += '</select>';
        div.innerHTML = dD;
        div.firstChild.onmousedown = div.firstChild.ondblclick = L.DomEvent.stopPropagation;
        return div;
    };
    legend.addTo(map);
}

function _i(map, gpx) {

    if (typeof _activeLegend !== "undefined")
        _activeLegend.remove();

    _activeLegend = L.control({ position: 'bottomright' });
    _activeLegend.onAdd = function (map) {
        const distance = (gpx.get_distance() / 1000).toFixed(2);
        const duration = gpx.get_duration_string_iso(gpx.get_total_time(), true);
        const pace = gpx.get_duration_string_iso(gpx.get_moving_pace(), true);
        const elGain = gpx.get_elevation_gain().toFixed(0);
        const elLoss = gpx.get_elevation_loss().toFixed(0);
        const elNet =  (gpx.get_elevation_gain() - gpx.get_elevation_loss()).toFixed(0);
        let s = '<ul class="info">';
        s += '<li>Distanz:&nbsp;' + distance + '&nbsp;km</li>';
        s += '<li>Dauer:&nbsp;' + duration + '</li>';
        s += '<li>Pace:&nbsp;' + pace + '/km</li>'
        s += '<li>Elevation:&nbsp;+' + elGain + '&nbsp;,';
        s += '-' + elLoss + '&nbsp;m';
        s += ' (net:&nbsp;' + elNet + '&nbsp;m)</li>';
        s += '</ul>';
        const div = L.DomUtil.create('div', 'l-info');
        div.innerHTML = s;
        div.firstChild.onmousedown = div.firstChild.ondblclick = L.DomEvent.stopPropagation;
        return div;
    };
    _activeLegend.addTo(map);
}

function loadGPX(newGPX) {
    _showLoadingIndicator();
    _activeRoute.remove();
    _activeRoute = _g(map, newGPX);
}

function _showLoadingIndicator() {
    document.getElementsByClassName('lds-facebook')[0].style.display = 'inline-block';
}

function _hideLoadingIndicator() {
    document.getElementsByClassName('lds-facebook')[0].style.display = 'none';
}