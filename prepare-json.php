<?php
class aFile {
    public $lFile = "";
    public $lName = "";
    function __construct($f) {
        $this->lFile = $f;
        $this->lName = rtrim(ltrim($f, 'gpx\/'), '.gpx');
    }
}


$files = glob("gpx/*.gpx");
$files = array_reverse($files);
$json = [];
foreach($files as $file) {
    $f = new aFile($file);
    $json[] = $f;
}

$j = json_encode($json);
file_put_contents('files.json', $j);