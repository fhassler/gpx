# l.florian-hassler.net 

My public page to share GPX-Routes my phone produces when I go out for a 
walk.

# Uses 

- Leaflet.js
- https://github.com/mpetazzoni/leaflet-gpx for reading and displaying 
  GPX-Tracks
- Openstreetmap


# TODO

* [ ] check the licenses of the used libraries
* [x]  Make CI read `gpx`-Dir and output to `files.json`
* [x] add loading-indicator while loading...GPX-Files sometimes are big
* [ ] add URL-Param to load specific GPX file
